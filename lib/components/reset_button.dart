import 'package:flutter/material.dart';

class ResetButton extends StatelessWidget {

  final Function resetFunction;

  const ResetButton({super.key , required this.resetFunction});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: const Icon(Icons.exposure_zero) ,
      onPressed: () => resetFunction()
    );
  }
}