import 'package:flutter/material.dart';

class IncrementButton extends StatelessWidget {
  final Function incrementFunction;

  const IncrementButton({super.key, required this.incrementFunction});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => incrementFunction(),
      child: const Icon(Icons.plus_one),
    );
  }
}