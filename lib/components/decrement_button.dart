import 'package:flutter/material.dart';

class DecrementButton extends StatelessWidget {

  final Function decrementFunction;

  const DecrementButton({super.key, required this.decrementFunction});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => decrementFunction(),
      child: const Icon(Icons.exposure_minus_1),
      );
  }
}