import 'package:flutter/material.dart';
import 'package:counter_app/components/decrement_button.dart';
import 'package:counter_app/components/increment_button.dart';
import 'package:counter_app/components/reset_button.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  // Funcion que incrementa en 1 el contador
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  // Funcion que decrementa en 1 al contador
  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  // Funcion que resetea el contador
  void _resetCounter() {
    setState(() {
      _counter = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),

      // * Coloca el FloatingActionButton en el centro de la pantalla con un espacio entre el borde inferior
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      // Agrupa los tres botones y los alinea dandoles espacio entre ellos
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          DecrementButton(decrementFunction: _decrementCounter),
          ResetButton(resetFunction: _resetCounter),
          IncrementButton(incrementFunction: _incrementCounter)
        ]
      )
    );
  }
}
